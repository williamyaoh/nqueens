# nqueens

A small demonstration of using the [Reanimate](https://hackage.haskell.org/package/reanimate)
library to produce animations programatically. Generates a GIF showing
how to use depth-first search to solve the [N-queens puzzle](https://en.wikipedia.org/wiki/Eight_queens_puzzle).

![](./8Queens.gif)

## Building and running

For when you want to generate the animation yourself, or make tweaks to it.

Along with [Stack](https://haskellstack.org) for building the Haskell code,
you'll need the following executables installed:

* ffmpeg >= 4.1.3
* gifsicle

Run the following commands from the top-level directory:

```bash
stack build nqueens:anim
stack exec anim render-final
```

This will render the raw output from the Reanimate library and do some
post-processing to produce the final GIF.

See the documentation in [`anim.hs`](https://gitlab.com/williamyaoh/nqueens/-/blob/master/src/bin/anim.hs)
for what other build commands are available.
