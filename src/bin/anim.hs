{-# LANGUAGE QuasiQuotes #-}

-- |
-- A build system for quickly generating our NQueens animation. All output goes
-- into the @_dist/@ directory.
--
-- Available commands:
--
-- * render, render-dev -- generate raw output from @reanimate@ as MP4, don't
--                         do any post-processing
-- * render-final -- generate the final GIF for distribution
-- * clean -- clean up @_dist/@ directory and Stack outputs
-- * lint -- check for lints using @hlint@ and @stylish-haskell@
-- * build -- build just the animation generator, but don't do any generation
--
-- Dependencies:
--
-- Along with Stack for building the Haskell code, you'll need the following
-- executables installed:
--
-- * ffmpeg >= 4.1.3
-- * gifsicle
--
-- If you want to lint, you'll also need to install:
--
-- * hlint
-- * stylish-haskell

module Main where

import Data.Foldable
import Data.String.Interpolate ( i )

import Development.Shake
import Development.Shake.Command
import Development.Shake.FilePath

main :: IO ()
main = do
  let outdir = "_dist" :: FilePath

  shakeArgs shakeOptions { shakeFiles = outdir, shakeVerbosity = Verbose } $ do
    want [ "render-dev" ]

    phony "clean" $ do
      putInfo "cleaning build directory..."
      cmd_ "stack clean"
      removeFilesAfter [i|#{outdir}/|] [ "//*" ]

    phony "lint" $ do
      haskell <- (fmap . fmap) ("src/" </>) (getDirectoryFiles "src" [ "//*.hs" ])
      traverse_ (cmd_ "hlint") haskell
      traverse_ (cmd_ "stylish-haskell" "-i") haskell

    let genName = [i|#{outdir}/nqueens-exe|] <.> exe
    let renderName = [i|#{outdir}/render_640x360|]

    phony "build" $ need [ genName ]

    phony "render" $ need [ "render-dev" ]

    -- We avoid doing any postprocessing on the `reanimate' output in development,
    -- to avoid making the render cycles any longer than they already are.
    phony "render-dev" $ need [ renderName -<.> "mp4" ]

    -- Once we actually want a final output to distribute, we need to crop and
    -- convert to a GIF, plus other compression bits.
    phony "render-final" $ need [ renderName -<.> "gif" ]

    renderName -<.> "gif" %> \out -> do
      need [ renderName -<.> "mp4" ]
      command_ [ Stdin "" ]  -- This empty `stdin' is necessary to get ffmpeg to not hang. ffmpeg pls
               "ffmpeg"
               [ "-i", renderName -<.> "mp4"
               , "-y", "-f", "gif"
               , "-filter_complex", "[0:v] crop=360:360:0:0,split [a][b]; [a] palettegen [p]; [b][p] paletteuse"
               , renderName -<.> "gif"
               ]
      command_ [] "gifsicle" [ "--batch", "-O3", "-i", renderName -<.> "gif", "--colors", "16" ]

    renderName -<.> "mp4" %> \out -> do
      need [ genName, "queen.svg" ]
      command_ [] genName [ "render", "-o", [i|./#{outdir}/render_640x360.mp4|]
                          , "--format", "mp4"
                          , "-w", "640", "-h", "360"
                          , "--fps", "24"
                          ]

    genName %> \_ -> do
      need [ "src/bin/nqueens.hs" ]
      command_ [] "stack" [ "build", "nqueens:nqueens-exe", "--copy-bins", "--local-bin-path", [i|./#{outdir}|] ]
