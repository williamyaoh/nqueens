{-# LANGUAGE MultiWayIf     #-}
{-# LANGUAGE PackageImports #-}
{-# LANGUAGE ViewPatterns   #-}

-- |
-- Remember when working with @reanimate@ that you __always__ have to
-- use Cartesian coordinates, not "computer rendering" coordinates.
-- The (1, 1) vector __always__ points to the top right.
--
-- Even if you use the SVG primitives from @reanimate-svg@, this rule
-- still holds.
--
-- Also, when working with SVG groups, elements that are specified last render
-- on top.

module Main where

import           "base" Data.Foldable         ( foldl' )
import           "containers" Data.Map.Strict ( Map )
import qualified "containers" Data.Map.Strict as Map

import "lens" Control.Lens hiding ( elements, transform )

import "linear" Linear.V2 ( V2(..) )

import "JuicyPixels" Codec.Picture.Types

import "reanimate" Reanimate

import "reanimate-svg" Graphics.SvgTree as SVG

data StepAction = Succeed | FailRow | FailCol
  deriving (Eq, Show)

type SolutionStep = (Map Int Int, Int, Int, StepAction)

main :: IO ()
main = do
  qsvg <- queen

  let solutionSteps = nqueensTrace 8
      staggeredSteps = splitAtEach [9, 72, 200, 955, 974] solutionSteps
      staggeredAnims = foldl' seqA (pause 0) . fmap (drawStep qsvg) <$> staggeredSteps
      overlays = [ pause 0, mkAnimation 0 (const ffw), mkAnimation 0 (const fffw), mkAnimation 0 (const fffw), mkAnimation 0 (const ffw), pause 0 ]
      durations = [ 1.0, 0.4, 0.1, 0.05, 0.4, 1.0 ]
      solutionAnim =
        foldl' (\anim1 (anim2, overlay, duration) ->
                 anim1 `seqA` adjustDuration (*duration) (anim2 `parA` overlay))
               (pause 0)
               (zip3 staggeredAnims overlays durations)

  reanimate $
    backgroundAnim `andThen` solutionAnim
      & pauseAtEnd 10
  where backgroundAnim :: Animation
        backgroundAnim = mkAnimation 0 (const bg)

--------------------
-- GENERIC ANIMATION STUFF
--------------------

-- |
-- A pure white background. Mostly for testing purposes or filling in dead space.
bg :: SVG.Tree
bg = RectangleTree $ defaultSvg
  & rectBottomLeft .~ (Num $ negate $ screenWidth / 2, Num $ negate $ screenHeight / 2)
  & rectHeight     ?~ Num screenHeight
  & rectWidth      ?~ Num screenWidth
  & fillOpacity    ?~ 1.0
  & fillColor      .~ pure (colorRGB 0xFF 0xFF 0xFF)

toBottomLeft :: SVG.Tree -> SVG.Tree
toBottomLeft svg =
  let (minX, minY, _, _) = boundingBox svg
      (viewboxX, viewboxY) = boardBottomLeft
  in translate (viewboxX - minX) (viewboxY - minY) svg

-- |
-- Get the centermost point of the SVG.
centerOf :: SVG.Tree -> (Double, Double)
centerOf svg =
  let (minX, minY, w, h) = boundingBox svg
  in (minX + w/2, minY + h/2)

-- |
-- @centerWithin a b@ will produce a new SVG with @a@ centered inside @b@. The
-- position of @b@ is unchanged.
centerWithin :: SVG.Tree -> SVG.Tree -> SVG.Tree
centerWithin a b =
  let (ax, ay) = centerOf a
      (bx, by) = centerOf b
  in mkGroup [ b, translate (bx - ax) (by - ay) a ]

-- |
-- @centerLeft a b@ will produce a new SVG with @a@ aligned on the left side of @b@,
-- with both images center-aligned vertically. The position of @b@ is unchanged.
centerLeft :: SVG.Tree -> SVG.Tree -> SVG.Tree
centerLeft a b =
  let (ax, ay) = centerOf a
      (bx, by) = centerOf b
      (_, _, aw, _) = boundingBox a
      (_, _, bw, _) = boundingBox b
  in mkGroup [ b, translate (bx-aw/2-bw/2-ax) (by-ay) a ]

-- |
-- Convenience function for testing, adds a rectangle around the passed-in SVG.
addBoundingBox :: SVG.Tree -> SVG.Tree
addBoundingBox svg =
  let (minX, minY, w, h) = boundingBox svg
  in mkGroup
       [ RectangleTree $ defaultSvg
           & rectBottomLeft .~ (Num minX, Num minY)
           & rectWidth      ?~ Num w
           & rectHeight     ?~ Num h
           & fillOpacity    ?~ 0.0
           & strokeOpacity  ?~ 1.0
           & strokeColor    .~ pure (colorRGB 0xFF 0x00 0x00)
           & strokeWidth    .~ pure (Num defaultStrokeWidth)
       , svg
       ]

-- |
-- Convenience function for testing, adds X and Y axes to make it easier to
-- locate things.
centerLines :: SVG.Tree
centerLines = mkGroup [ PathTree xAxis, PathTree yAxis ]
  where baseAxis = defaultSvg
          & strokeWidth   .~ pure (Num defaultStrokeWidth)
          & strokeColor   .~ pure (colorRGB 0xFF 0x00 0x00)
          & strokeOpacity ?~ 1.0
          & fillOpacity   ?~ 0.0
        xAxis = baseAxis & pathDefinition .~
          [ MoveTo OriginAbsolute [V2 0 4.5]
          , LineTo OriginAbsolute [V2 0 (-4.5)]
          ]
        yAxis = baseAxis & pathDefinition .~
          [ MoveTo OriginAbsolute [V2 (-8) 0]
          , LineTo OriginAbsolute [V2 8 0]
          ]

colorRGB :: Pixel8 -> Pixel8 -> Pixel8 -> SVG.Texture
colorRGB r g b = ColorRef $ PixelRGBA8 r g b 0xFF

-- |
-- Because of the inversion that @reanimate@ uses to achieve Cartesian coordinates,
-- `rectUpperLeftCorner' becomes a misnomer. Position rectangles by their bottom
-- left instead.
rectBottomLeft :: HasRectangle a => Lens' a Point
rectBottomLeft = rectUpperLeftCorner

cubicBezierS :: (Double, Double, Double, Double) -> Signal
cubicBezierS (x1, x2, x3, x4) s =
  let s2 = s*s
      s3 = s2*s
      ms = 1-s
      ms2 = ms*ms
      ms3 = ms2*ms
  in x1*ms3 + 3*x2*ms2*s + 3*x3*ms*s2 + x4*s3

splitAtEach :: [Int] -> [a] -> [[a]]
splitAtEach _ [] = []
splitAtEach [] l = [l]
splitAtEach (ix:ixs) l =
  let (pre, post) = splitAt ix l
  in pre : splitAtEach (fmap (flip (-) ix) ixs) post

--------------------
-- DRAWING THE BOARD AND PIECES
--------------------

-- |
-- An overlay for tiles that are blocked by other queens. Translucent.
badTile :: SVG.Tree
badTile = RectangleTree $ defaultSvg
  & rectWidth     ?~ Num tileWidth
  & rectHeight    ?~ Num tileWidth
  & strokeOpacity ?~ 0.0
  & fillOpacity   ?~ 0.3
  & fillColor     .~ pure boardBad

boardDark :: SVG.Texture
boardDark = colorRGB 0x88 0x77 0xB7

boardLight :: SVG.Texture
boardLight = colorRGB 0xEF 0xEF 0xEF

-- |
-- Color for spaces that are blocked by other queens.
boardBad :: SVG.Texture
boardBad = colorRGB 0xFF 0x00 0x00

-- |
-- This box isn't rendered at all, but we use it to center other SVGs within,
-- so that they're exactly the size of a chessboard tile, easing positioning later.
tileBox :: SVG.Tree
tileBox = RectangleTree $ defaultSvg
  & strokeOpacity ?~ 0.0
  & fillOpacity   ?~ 0.0
  & rectWidth     ?~ Num (boardWidth / 8)
  & rectHeight    ?~ Num (boardWidth / 8)

-- |
-- For crossing out queens that are conflicting.
cross :: SVG.Tree
cross = toBottomLeft $ flip centerWithin tileBox $ PathTree $ defaultSvg
  & strokeWidth    .~ pure (Num 0.03)
  & strokeColor    .~ pure (colorRGB 0x00 0x00 0x00)
  & strokeOpacity  ?~ 1.0
  & strokeLineJoin .~ pure JoinRound
  & fillOpacity    ?~ 1.0
  & fillColor      .~ pure (colorRGB 0xE5 0x4B 0x4B)
  & transform      ?~ [ Rotate 45 Nothing ]
  & pathDefinition .~
    [ MoveTo OriginAbsolute [V2 0 0]
    , MoveTo OriginRelative [V2 (crossBranchWidth/2) 0]
    , LineTo OriginRelative
        [ V2 0 crossBranchLength
        , V2 crossBranchLength 0
        , V2 0 crossBranchWidth
        , V2 (-crossBranchLength) 0
        , V2 0 crossBranchLength
        , V2 (-crossBranchWidth) 0
        , V2 0 (-crossBranchLength)
        , V2 (-crossBranchLength) 0
        , V2 0 (-crossBranchWidth)
        , V2 crossBranchLength 0
        , V2 0 (-crossBranchLength)
        ]
    , EndPath
    ]
  where crossBranchWidth = tileWidth / 7
        crossBranchLength = tileWidth / 2.5

-- |
-- A triangle for showing that we're fast-forwarding.
ffwTriangle :: SVG.Tree
ffwTriangle = toBottomLeft $ PathTree $ defaultSvg
  & strokeWidth    .~ pure (Num defaultStrokeWidth)
  & strokeColor    .~ pure (colorRGB 0x00 0x00 0x00)
  & strokeOpacity  ?~ 0.7
  & strokeLineJoin .~ pure JoinRound
  & fillOpacity    ?~ 0.7
  & fillColor      .~ pure (colorRGB 0xFF 0xFF 0xFF)
  & transform      ?~ [ Rotate 270 Nothing ]
  & pathDefinition .~
    [ MoveTo OriginAbsolute [V2 0 0]
    , LineTo OriginRelative
      [ V2 sideLength 0
      , V2 (-(sideLength/2)) (sideLength*0.55)
      ]
    , EndPath
    ]
  where sideLength = tileWidth / 1.3

ffw :: SVG.Tree
ffw = translate (desiredX-iconX) (desiredY-iconY) icon
  where icon = ffwTriangle `centerLeft` ffwTriangle
        (iconX, iconY) = centerOf icon
        (desiredX, desiredY) = bimap (+ (tileWidth*7.3)) (+ (tileWidth*7.3)) boardBottomLeft

fffw :: SVG.Tree
fffw = ffwTriangle `centerLeft` ffw

-- |
-- The whole chessboard background. Renders on the left side of the output.
chessboard :: SVG.Tree
chessboard = mkGroup $ fmap byTileNo [0..63]
  where (boardMinX, boardMinY) = boardBottomLeft
        baseTileSVG = defaultSvg
          & rectWidth     ?~ Num tileWidth
          & rectHeight    ?~ Num tileWidth
          & fillOpacity   ?~ 1.0
          & strokeOpacity ?~ 0.0
        darkTile = baseTileSVG
          & fillColor .~ pure boardDark
        lightTile = baseTileSVG
          & fillColor .~ pure boardLight

        byTileNo :: Int -> SVG.Tree
        byTileNo tileNo =
          let base = if tileNo `mod` 2 == 0 then darkTile else lightTile
              (y, x) = tileNo `divMod` 8
              -- For the positions, we want to 'alternate' the tile placing
              -- direction for each row.
              pos = ( Num $ (fromIntegral x * tileWidth) * (if y `mod` 2 == 0 then 1 else -1)
                        + boardMinX
                        + (if y `mod` 2 == 0 then 0 else tileWidth * 7)
                    , Num $ fromIntegral y * tileWidth + boardMinY
                    )
          in RectangleTree $ base
               & rectBottomLeft .~ pos

queen :: IO SVG.Tree
queen = do
  Just queenDoc <- SVG.loadSvgFile "queen.svg"

  let queen = mkGroup (queenDoc ^. elements)
        & scale 0.025
        & rotate 180

  pure $ centerWithin queen tileBox
    & toBottomLeft

boardBottomLeft :: (Double, Double)
boardBottomLeft =
  (negate $ screenWidth / 2, negate $ screenHeight / 2)

-- | And height, since it's square.
boardWidth :: Double
boardWidth = screenHeight

-- | And height, since tiles are square.
tileWidth :: Double
tileWidth = boardWidth / 8

-- |
-- Given a board position (where the origin is the top left, (1, 1)), give back
-- the Cartesian coordinates of the bottom left of that tile.
realCoordsAt :: (Int, Int) -> (Double, Double)
realCoordsAt (fromIntegral -> x, fromIntegral -> y) =
  ((x-1) * tileWidth, boardWidth - y*tileWidth)

-- |
-- In terms of board position. Top left of the board is (1, 1). Indices are 1-8,
-- to better facilitate positioning queens.
svgAt :: SVG.Tree -> (Int, Int) -> SVG.Tree
svgAt svg (realCoordsAt -> (x, y)) =
  toBottomLeft svg
    & translate x y

drawQueenDrop :: SVG.Tree -> (Int, Int) -> Animation
drawQueenDrop queenSVG (realCoordsAt -> (x, y)) =
  mkAnimation 0.5 (\t -> toBottomLeft queenSVG & translate x (y + tileWidth*(1-t)))
    & signalA (cubicBezierS (0.0, 0.87, 0.97, 1.0))

drawStep :: SVG.Tree -> SolutionStep -> Animation
drawStep queenSVG step@(positions, currQueen, currPos, action) =
  mkAnimation 1.0 (const $ mkGroup (chessboard : (svgAt queenSVG <$> Map.toAscList positions)))
    `parA` queenAnim
  where queenAnim :: Animation
        queenAnim = case action of
          Succeed -> drawQueenDrop queenSVG (currQueen, currPos)
                       `andThen` pause 1.0
          FailRow -> drawQueenDrop queenSVG (currQueen, currPos)
                       `andThen` buzzBadQueens (conflictingQueens step)
          FailCol -> mkAnimation 0.5 (const (svgAt queenSVG (currQueen, currPos-1)))
                       `andThen` buzzCol currQueen
                       `seqA`    drawQueenDrop queenSVG (currQueen, currPos)

-- |
-- Draw red crosses on top of each queen that's conflicting.
buzzBadQueens :: [(Int, Int)] -> Animation
buzzBadQueens positions =
  pause 0.05
    `seqA` mkAnimation 0.1 (const crosses)
    `seqA` pause 0.1
    `seqA` mkAnimation 0.25 (const crosses)
  where crosses :: SVG.Tree
        crosses = mkGroup $ fmap (svgAt $ centerWithin cross badTile) positions

-- |
-- If we can't find a single good position for a queen in a column, draw
-- crosses on top of the whole thing.
buzzCol :: Int -> Animation
buzzCol colNo =
  pause 0.05
    `seqA` mkAnimation 0.1 (const buzz)
    `seqA` pause 0.1
    `seqA` mkAnimation 0.25 (const buzz)
  where buzz :: SVG.Tree
        buzz = mkGroup $
          fmap (svgAt badTile) [(colNo, i) | i <- [1..8]]
            ++ [svgAt cross (colNo, 8)]

-- |
-- What queens are conflicting with the current one? Give back their board
-- positions.
conflictingQueens :: SolutionStep -> [(Int, Int)]
conflictingQueens (positions, currQueen, currPos, _) =
  filter (conflict (currQueen, currPos)) (Map.toAscList positions)

--------------------
-- NQUEENS SOLUTION
--------------------

conflict :: (Int, Int) -> (Int, Int) -> Bool
conflict (x1, y1) (x2, y2) =
  if | y2 == y1                       -> True
     | abs (x2 - x1) == abs (y2 - y1) -> True
     | otherwise                      -> False

conflicts :: (Int, Int) -> Map Int Int -> Bool
conflicts queen positions = any (conflict queen) (Map.toAscList positions)

-- |
-- While this function isn't used, it's here as a reference implementation
-- of how to do backtracking search using continuations, without the extra
-- noise of keeping track of function traces.
nqueensCC :: Int -> Int -> Int -> Map Int Int -> (Map Int Int -> r) -> r -> r
nqueensCC maxQueens currQueen currPos positions onSucc onFail =
  if | currQueen > maxQueens -> onSucc positions
     | currPos > maxQueens -> onFail
     | not (conflicts (currQueen, currPos) positions) ->
       nqueensCC maxQueens (currQueen + 1) 1 (Map.insert currQueen currPos positions) onSucc
         (nqueensCC maxQueens currQueen (currPos + 1) positions onSucc onFail)
     | otherwise -> nqueensCC maxQueens currQueen (currPos + 1) positions onSucc onFail

nqueens :: Int -> Maybe (Map Int Int)
nqueens numQueens = nqueensCC
  numQueens
  1
  1
  Map.empty
  Just
  Nothing

-- |
-- Trace the execution and see what it's doing at each step, so we can
-- animate it. Note that for 8 queens, this list is pretty long, so we
-- need to make sure our animations are short.
nqueensTraceCC :: Int
               -> Int
               -> Int
               -> Map Int Int
               -> ([SolutionStep] -> r)
               -> ([SolutionStep] -> r)
               -> r
nqueensTraceCC maxQueens currQueen currPos positions onSucc onFail =
  if | currQueen > maxQueens -> onSucc []
     | currPos > maxQueens -> onFail [(positions, currQueen, currPos, FailCol)]
     | not (conflicts (currQueen, currPos) positions) ->
       nqueensTraceCC maxQueens (currQueen + 1) 1 (Map.insert currQueen currPos positions)
         (\trace -> onSucc $ (positions, currQueen, currPos, Succeed) : trace)
         (\t -> nqueensTraceCC maxQueens currQueen (currPos + 1) positions
                 (\trace -> onSucc $ (positions, currQueen, currPos, Succeed) : (t ++ trace))
                 (\trace -> onFail $ (positions, currQueen, currPos, Succeed) : (t ++ trace)))
     | otherwise -> nqueensTraceCC maxQueens currQueen (currPos + 1) positions
         (\trace -> onSucc $ (positions, currQueen, currPos, FailRow) : trace)
         (\trace -> onFail $ (positions, currQueen, currPos, FailRow) : trace)

-- |
-- Solve NQueens for a given board size, and give back the steps taken to find
-- a solution, if any.
nqueensTrace :: Int -> [SolutionStep]
nqueensTrace numQueens = nqueensTraceCC
  numQueens
  1
  1
  Map.empty
  id
  id
